package com.afiinfotech.solverfileuploader.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.afiinfotech.solverfileuploader.Model.SelectedFileModel;
import com.afiinfotech.solverfileuploader.Model.UploadProgressData;
import com.afiinfotech.solverfileuploader.R;
import com.afiinfotech.solverfileuploader.Utilis.FileUploadThread;
import com.afiinfotech.solverfileuploader.Utilis.UploadProgressListner;
import com.afiinfotech.solverfileuploader.adapter.UploadListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/**
 * Created by afi-mac-001 on 07/06/16.
 */
public class UploadDialog extends DialogFragment implements UploadProgressListner {

    ExecutorService executorService;
    List<SelectedFileModel> selectedFileModels;
    Button positiveButton;
    private Handler uiHandler = new Handler();
    private RecyclerView recyclerViewUploads;
    private int lastThreadIndex = -1;
    private Map<Integer, Boolean> threadSuccessStatusMap = new HashMap<>();
    private String patientId;
    private String userId;
    private boolean onDialogPaused = false;

    private DialogActionListner dialogActionListner;

    public UploadDialog() {

    }

    public static UploadDialog create(ExecutorService executorService, List<SelectedFileModel> selectedFileModels, String patientId, String userId, DialogActionListner dialogActionListner) {
        UploadDialog uploadDialog = new UploadDialog();
        uploadDialog.executorService = executorService;
        uploadDialog.selectedFileModels = selectedFileModels;
        uploadDialog.patientId = patientId;
        uploadDialog.userId = userId;
        uploadDialog.dialogActionListner = dialogActionListner;
        return uploadDialog;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getContext())
                .setTitle("Uploading")
                .setView(getDialogView())
                .setCancelable(false)
                .setNegativeButton("Cancel All", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!this.onDialogPaused) {

            this.onDialogPaused = false;
            upload();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        this.onDialogPaused = true;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setCancelable(false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        AlertDialog d = (AlertDialog) getDialog();
        if (d != null) {
            positiveButton = d.getButton(Dialog.BUTTON_NEGATIVE);
            positiveButton.setTag(false);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if ((Boolean) positiveButton.getTag()) {
                        positiveButton.setText("Cancel All");
                        Toast.makeText(UploadDialog.this.getContext(), "Retry All", Toast.LENGTH_SHORT).show();
                        positiveButton.setTag(false);
                    } else {
                        new AlertDialog.Builder(getContext())
                                .setTitle("Alert")
                                .setMessage("Cancel all uploading tasks")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        UploadDialog.this.dismiss();
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .create()
                                .show();
                    }
                }
            });
        }

    }

    private void upload() {

        List<UploadProgressData> uploadProgressDatas = new ArrayList<>();
        for (SelectedFileModel selectedFileModel : this.selectedFileModels) {

            int index = this.selectedFileModels.indexOf(selectedFileModel);
            selectedFileModel.setId(index);
            UploadProgressData uploadProgressData = new UploadProgressData();
            uploadProgressData.setProgress(0);
            uploadProgressData.setFileName(selectedFileModel.getFileName());
            uploadProgressData.setFileType(selectedFileModel.getFileMimeType());
            uploadProgressData.setItemId(index);
            uploadProgressDatas.add(uploadProgressData);
        }

        updateView(uploadProgressDatas);


        for (SelectedFileModel selectedFileModel : this.selectedFileModels) {

            executorService.execute(new FileUploadThread(uiHandler, this, selectedFileModel, this.userId, this.patientId, getActivity()));
            lastThreadIndex = selectedFileModel.getId();
        }
    }


    private void updateView(List<UploadProgressData> uploadProgressDatas) {

        recyclerViewUploads.setAdapter(new UploadListAdapter(uploadProgressDatas));
    }

    private View getDialogView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_upload_files, null);
        recyclerViewUploads = (RecyclerView) view.findViewById(R.id.rvListUploads);
        recyclerViewUploads.setHasFixedSize(true);
        recyclerViewUploads.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    public void onItemCompleted(int id) {
        ((UploadListAdapter) recyclerViewUploads.getAdapter()).onItemCompleted(id);
        threadSuccessStatusMap.put(id, true);
        if (id == lastThreadIndex) {
            if (isEveryThreadSucceeded()) {
                try {
                    UploadDialog.this.dismiss();
                    dialogActionListner.onCompletedAllUploads();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                positiveButton.setText("Retry All");
                positiveButton.setTag(true);
                dialogActionListner.onFailed();
            }
        }
    }

    @Override
    public void onItemProgressChanged(int id, int progress) {
        ((UploadListAdapter) recyclerViewUploads.getAdapter()).onItemProgressChanged(id, progress);
    }

    @Override
    public void onItemUploadError(int id) {
        ((UploadListAdapter) recyclerViewUploads.getAdapter()).onItemUploadError(id);
        threadSuccessStatusMap.put(id, false);
        if (id == lastThreadIndex) {
            if (isEveryThreadSucceeded()) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UploadDialog.this.dialogActionListner.onCompletedAllUploads();
                        UploadDialog.this.dismiss();
                    }
                }, 600);
            } else {
                this.dialogActionListner.onFailed();
                positiveButton.setText("Retry All");
                positiveButton.setTag(true);
            }
        }

    }

    /**
     * @return
     */
    private boolean isEveryThreadSucceeded() {

        for (Map.Entry<Integer, Boolean> set : threadSuccessStatusMap.entrySet()) {
            if (!set.getValue()) {
                return false;
            }
        }
        return true;
    }

    public interface DialogActionListner {
        void onCompletedAllUploads();

        void onFailed();
    }
}
