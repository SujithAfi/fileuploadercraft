package com.afiinfotech.solverfileuploader;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afiinfotech.solverfileuploader.Model.AuthenticationRespose;
import com.afiinfotech.solverfileuploader.Utilis.Utilities;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Handler uiHandler = new Handler();
    private SharedPreferences prefs;
    private EditText etUserName;
    private EditText etUserPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        etUserName = (TextInputEditText) findViewById(R.id.edtTxtUsername);
        etUserPassword = (TextInputEditText) findViewById(R.id.edtTxtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utilities.hideKeyboard(LoginActivity.this);
                if (Validate()) {
                    login(etUserName.getText().toString(), etUserPassword.getText().toString());
                }

            }
        });

        prefs = Utilities.getSharedPreferences(this);
        String userid = prefs.getString("user_id", null);
        String userName = prefs.getString("user_name", null);

        if (userid != null && userName != null) {

            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }


    }

    private boolean Validate() {

        boolean validate = true;

        if (etUserName.getText().toString().trim().length() == 0) {
            etUserName.setError("Enter User Name");
            etUserName.requestFocus();
            validate = false;
        } else if (etUserPassword.getText().toString().trim().length() == 0) {

            etUserPassword.setError("Enter Password");
            etUserPassword.requestFocus();
            validate = false;
        }


        return validate;
    }

    private void login(final String username, final String password) {

        if (isSoftEng(username, password)) {

            Intent intent = new Intent(this, ConfigurationActivity.class);
            startActivity(intent);

            return;
        }

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Authenticating...!");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {

                Map<String, String> varivales = new HashMap<>();
                varivales.put("username", username);
                varivales.put("password", password);
                try {


                    final Response<AuthenticationRespose> resp = Utilities.getWebApi(LoginActivity.this).login(varivales).execute();

                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();

                            try {
                                if (resp != null) {
                                    if (resp.body() != null) {
                                        if (resp.body().isAuthenticated()) {

                                            Toast.makeText(LoginActivity.this, "Authenticated", Toast.LENGTH_SHORT).show();

                                            saveCredentials(resp.body());

                                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                            startActivity(intent);
                                            finish();


                                        } else {
                                            Utilities.showSnackBar("User Name or Password incorrect.", LoginActivity.this);
                                        }
                                    } else {
                                        Utilities.showSnackBar("Network error", LoginActivity.this);
                                    }
                                } else {
                                    Utilities.showSnackBar("Network error", LoginActivity.this);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });


                } catch (IOException e) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Utilities.showSnackBar("Network error", LoginActivity.this);
                        }
                    });
                }


            }
        }).start();

    }

    private boolean isSoftEng(String username, String password) {

        try {
            Calendar now = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMkkmm");
            String nowString = dateFormat.format(now.getTime()) + "THANKGOD";
            boolean usernameMatch = username.equalsIgnoreCase("softeng");
            boolean passwordMatch = password.equalsIgnoreCase(nowString);
            return usernameMatch && passwordMatch;
        } catch (Exception e) {
            return false;
        }
    }

    private void saveCredentials(AuthenticationRespose body) {

        SharedPreferences.Editor pref = Utilities.getSharedPreferences(this).edit();

        pref.putString("user_id", body.getUserId());
        pref.putString("user_name", body.getUserName());


        if (body.getFileTypesList() != null) {
            Utilities.saveFileTypes(pref, body.getFileTypesList());
        }
        pref.commit();
    }


}
