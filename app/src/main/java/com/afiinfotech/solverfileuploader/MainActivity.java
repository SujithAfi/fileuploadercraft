package com.afiinfotech.solverfileuploader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afiinfotech.solverfileuploader.Model.PatientData;
import com.afiinfotech.solverfileuploader.Model.SelectedFileModel;
import com.afiinfotech.solverfileuploader.Utilis.Utilities;
import com.afiinfotech.solverfileuploader.adapter.MainPageFragmentAdapter;
import com.afiinfotech.solverfileuploader.dialogs.UploadDialog;
import com.afiinfotech.solverfileuploader.fragments.FileFragment;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements UploadDialog.DialogActionListner, MainPageFragmentAdapter.DialogActionListner {

    public boolean showcompleteFlag = true;
    boolean doubleBackToExitPressedOnce = false;
    private ViewPager viewPager;
    private SmartTabLayout tabLayout;
    private PatientData patientData = null;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private CoordinatorLayout container;
    private FileUploadApplication fileUploadApplication;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.viewpgerMain);
        tabLayout = (SmartTabLayout) findViewById(R.id.tabLayout);
        container = (CoordinatorLayout) findViewById(R.id.cl_container);

        fileUploadApplication = (FileUploadApplication) this.getApplication();

        showcompleteFlag = true;

        getSupportActionBar().setTitle("File uploader");
        getSupportActionBar().setSubtitle("User :" + Utilities.getSharedPreferences(this).getString("user_name", ""));

        getSupportActionBar().setHomeButtonEnabled(true);

        if (savedInstanceState != null) {
            MainPageFragmentAdapter adapter = new MainPageFragmentAdapter(getSupportFragmentManager(), this);
            int tabsize = savedInstanceState.getInt("tabSize");
            adapter.setTabSize(tabsize);
            viewPager.setAdapter(adapter);
        } else {
            viewPager.setAdapter(new MainPageFragmentAdapter(getSupportFragmentManager(), this));
        }

        tabLayout.setViewPager(viewPager);

        findViewById(R.id.btnLoad).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utilities.hideKeyboard(MainActivity.this);
                if (((EditText) findViewById(R.id.edtTxtPatientId)).getText().toString().trim().length() > 0) {
                    if(Utilities.isNetworkConnected(MainActivity.this))
                        loadData(((EditText) findViewById(R.id.edtTxtPatientId)).getText().toString());
                    else
                        Utilities.showSnackBar(getResources().getString(R.string.no_connectivity), MainActivity.this);
                }
                else {
                    ((EditText) findViewById(R.id.edtTxtPatientId)).setError("Enter Patient file id");
                    findViewById(R.id.edtTxtPatientId).requestFocus();
                    Utilities.showSnackBar("Enter Patient file id", MainActivity.this);
                }

            }
        });


        findViewById(R.id.addAttachmentButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainPageFragmentAdapter) viewPager.getAdapter()).addTab();
                viewPager.setCurrentItem(((MainPageFragmentAdapter) viewPager.getAdapter()).getTabSize() - 1, true);
                tabLayout.setViewPager(viewPager);
            }
        });


        findViewById(R.id.btnUpload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainPageFragmentAdapter) viewPager.getAdapter()).Update();

                Utilities.hideKeyboard(MainActivity.this);
                showcompleteFlag = true;
                if (patientData == null) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Validation error")
                            .setMessage("Patient not selected,please load patient data by providing patient id")
                            .setPositiveButton("ok", null)
                            .setCancelable(false)
                            .create()
                            .show();
                } else {

                    if (viewPager.getAdapter() != null) {
                        List<SelectedFileModel> selectedFileModels = ((MainPageFragmentAdapter) viewPager.getAdapter()).getFileModelList();
                        if (selectedFileModels.isEmpty()) {
                            new AlertDialog.Builder(MainActivity.this)
                                    .setTitle("No attachment selecled")
                                    .setMessage("Please select an attachment")
                                    .setPositiveButton("OK", null)
                                    .create()
                                    .show();
                        } else {

                            if(Utilities.isNetworkConnected(MainActivity.this))
                                uploadFiles(selectedFileModels);
                            else
                                Utilities.showSnackBar(getResources().getString(R.string.no_connectivity), MainActivity.this);
                        }
                    }

                }
            }
        });

        findViewById(R.id.btnDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showcompleteFlag = false;
                removeSelectedAttachment();
            }
        });

        Utilities.hideKeyboard(MainActivity.this);

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        int tabsize = ((MainPageFragmentAdapter) viewPager.getAdapter()).getTabSize();

        outState.putInt("tabSize", tabsize);

    }

    private void removeSelectedAttachment() {

        int selectedIndex = viewPager.getCurrentItem();
        ((MainPageFragmentAdapter) viewPager.getAdapter()).removeFragmentAtIndex(selectedIndex);
        tabLayout.setViewPager(viewPager);

    }

    private void uploadFiles(List<SelectedFileModel> selectedFileModels) {

        UploadDialog.create(executorService,
                selectedFileModels,
                ((EditText) findViewById(R.id.edtTxtPatientId)).getText().toString(),
                Utilities.getSharedPreferences(this).getString("user_id", ""), this)
                .show(getSupportFragmentManager(), "Upload Dialog");
    }


    private void loadData(final String patiendId) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Fetching user data");
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    final Response<PatientData> response = Utilities.getWebApi(MainActivity.this).getPatientData(patiendId).execute();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();

                            if (response == null) {
                                Toast.makeText(MainActivity.this, "Network error", Toast.LENGTH_SHORT).show();

                            } else if (response.body() == null) {
                                Toast.makeText(MainActivity.this, "Network error", Toast.LENGTH_SHORT).show();

                            } else if (response.body().getPatientId() == null) {
                                Toast.makeText(MainActivity.this, "Invalid patient id", Toast.LENGTH_SHORT).show();
                            } else {
                                initUiWithData(response.body());
                            }

                        }
                    });

                } catch (IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                }
            }
        }).start();

    }

    private void initUiWithData(PatientData body) {

        this.patientData = body;

        ((TextView) findViewById(R.id.txtUserId)).setText(body.getUserid());
        ((TextView) findViewById(R.id.txtUsername)).setText(body.getPatientName());
        ((TextView) findViewById(R.id.txtGender)).setText(body.getPatientSex().contains("F") ? "Female" : "Male");
        findViewById(R.id.txtUsername).setSelected(true);

    }

    @Override
    public void onCompletedAllUploads() {
        if (showcompleteFlag)
            Utilities.showSnackBar("Completed all uploads", MainActivity.this);
        removeAllFileFragments();
    }

    @Override
    public void onFailed() {

    }

    private void removeAllFileFragments() {
        ((MainPageFragmentAdapter) viewPager.getAdapter()).clearFragments();
        viewPager.removeAllViews();
        viewPager.setAdapter(null);
        viewPager.setAdapter(new MainPageFragmentAdapter(getSupportFragmentManager(), this));
        tabLayout.setViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                SharedPreferences.Editor prefEditor = Utilities.getSharedPreferences(MainActivity.this).edit();
                                prefEditor.remove("user_id");
                                prefEditor.remove("user_name");
                                prefEditor.commit();
                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Logout");
                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();


                return true;
            case R.id.action_photo:
                Bundle b = new Bundle();
                if (((EditText) findViewById(R.id.edtTxtPatientId)).getText().toString().trim().length() > 0)
                    b.putString("pID", ((EditText) findViewById(R.id.edtTxtPatientId)).getText().toString().trim());
                b.putString("uID", ((TextView) findViewById(R.id.txtUserId)).getText().toString().trim());
                b.putString("pName", ((TextView) findViewById(R.id.txtUsername)).getText().toString().trim());
                b.putString("pGender", ((TextView) findViewById(R.id.txtGender)).getText().toString().trim());
                Intent i = new Intent(MainActivity.this, UploadUserPhotoActivity.class);
                i.putExtra("bundle", b);
                startActivity(i);
                return true;

            case R.id.action_idcard:
                Bundle b1 = new Bundle();
                if (((EditText) findViewById(R.id.edtTxtPatientId)).getText().toString().trim().length() > 0)
                    b1.putString("pID", ((EditText) findViewById(R.id.edtTxtPatientId)).getText().toString().trim());
                b1.putString("uID", ((TextView) findViewById(R.id.txtUserId)).getText().toString().trim());
                b1.putString("pName", ((TextView) findViewById(R.id.txtUsername)).getText().toString().trim());
                b1.putString("pGender", ((TextView) findViewById(R.id.txtGender)).getText().toString().trim());
                Intent j = new Intent(MainActivity.this, UploadIDCardActivity.class);
                j.putExtra("bundle", b1);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Utilities.showSnackBar("Please click BACK again to exit", MainActivity.this);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        try {
            if (requestCode == FileFragment.TAKE_PICTURE && resultCode == Activity.RESULT_OK) {


                if (fileUploadApplication.getOnCapturCompleteListener() != null)
                    fileUploadApplication.getOnCapturCompleteListener().onComplete();

            }
            super.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
