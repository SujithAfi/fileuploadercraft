package com.afiinfotech.solverfileuploader.fragments;


import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afiinfotech.solverfileuploader.FileUploadApplication;
import com.afiinfotech.solverfileuploader.Model.SelectedFileModel;
import com.afiinfotech.solverfileuploader.R;
import com.afiinfotech.solverfileuploader.Utilis.ImageLoaderThread;
import com.afiinfotech.solverfileuploader.Utilis.PostCaptureListener;
import com.afiinfotech.solverfileuploader.Utilis.Utilities;
import com.nononsenseapps.filepicker.FilePickerActivity;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FileFragment extends Fragment {


    public static final int TAKE_PICTURE = 1;
    private static final int SELECT_PICTURE = 22;
    private static final int PICK_FILE_REQUEST_CODE = 23;
    private static final int FILE_TYPE_IMAGE = 1;
    private static final int FILE_TYPE_IMAGE_JPG = 41;
    private static final int FILE_TYPE_IMAGE_PNG = 42;
    private static final int FILE_TYPE_IMAGE_GIF = 43;
    private static final int FILE_TYPE_VIDEO = 2;
    private static final int FILE_TYPE_PDF = 3;
    private static final int FILE_TYPE_WORD = 4;
    private static final int FILE_TYPE_TXT = 5;
    private static final int FILE_TYPE_UNKNOWN = 122;
    public int position;
    boolean isFileSelected = false;
    String description = "";
    int selectedItemposition = 0;
    private String idString;
    private ImageView imgFileTypeIcon;
    private TextView txtFileName;
    private TextView txtFilePath;
    private Uri imageUri;
    private RelativeLayout fileOptionsContainer;
    private LinearLayout fileDetailsContainer;
    private String type;
    private int fileType = -1;
    private List<com.afiinfotech.solverfileuploader.Model.FileTypes> fileTypesList;
    private String fileName = "";
    private String filePath = "";
    private FragmentIneractor fragmentIneractor;
    private String pictureImagePath;
    private FileUploadApplication fileUploadApplication;

    private PostCaptureListener onPostCaptureListener = new PostCaptureListener() {
        @Override
        public void onComplete() {


            try {
                if (pictureImagePath != null) {
                    isFileSelected = true;
                    fileType = FILE_TYPE_IMAGE;
                    filePath = pictureImagePath;
                    togglePlaneVisibility();
                    pictureImagePath = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    };
    private String docType = "";

    public FileFragment() {
        // Required empty public constructor
    }


    public void setFragmentIneractor(FragmentIneractor fragmentIneractor) {
        this.fragmentIneractor = fragmentIneractor;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fileUploadApplication = (FileUploadApplication) getActivity().getApplication();
        fileUploadApplication.setOnCaptureCompleteListener(onPostCaptureListener);
        return inflater.inflate(R.layout.fragment_file, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        Log.e("onActivityCreated " + position, savedInstanceState == null ? "null" : "NOT NULL");

        if (savedInstanceState != null) {
            String idenrifier = savedInstanceState.getString("idString");

            this.idString = idenrifier;

            if (this.fragmentIneractor == null) {
                isFileSelected = savedInstanceState.getBoolean("isFileSelected");

                description = savedInstanceState.getString("description");
                ((TextInputEditText) getView().findViewById(R.id.edtTxtDescription)).setText(savedInstanceState.getString("description"));

                this.docType = savedInstanceState.getString("docType");
                this.filePath = savedInstanceState.getString("filePath");
            } else if (!fragmentIneractor.isIdRecycled(idenrifier)) {
                isFileSelected = savedInstanceState.getBoolean("isFileSelected");

                description = savedInstanceState.getString("description");
                ((TextInputEditText) getView().findViewById(R.id.edtTxtDescription)).setText(savedInstanceState.getString("description"));

                this.fileType = savedInstanceState.getInt("fileType");
                this.filePath = savedInstanceState.getString("filePath");
                this.docType = savedInstanceState.getString("docType");
            } else {
            }
        }

        fileOptionsContainer = (RelativeLayout) getView().findViewById(R.id.fileOptionsContainer);
        fileDetailsContainer = (LinearLayout) getView().findViewById(R.id.fileDetailsContainer);

        imgFileTypeIcon = (ImageView) getView().findViewById(R.id.imgFileTypeIcon);
        txtFileName = (TextView) getView().findViewById(R.id.txtFileName);
        txtFilePath = (TextView) getView().findViewById(R.id.txtFilePath);

        togglePlaneVisibility();

        fileTypesList = Utilities.getFileTypes(getContext());

        List<String> datas = new ArrayList<>();

        for (com.afiinfotech.solverfileuploader.Model.FileTypes fileTypesItem : fileTypesList) {
            datas.add(fileTypesItem.getDocumentTypeName());

        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, datas);

        ((AppCompatSpinner) getView().findViewById(R.id.spinnerFileType)).setAdapter(adapter);

        getView().findViewById(R.id.btnOpenCamera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });

        getView().findViewById(R.id.btnOpenGallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openGallery();

            }
        });

        getView().findViewById(R.id.btnOpenFileManager).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                opneFilePicker();

            }
        });

        ((AppCompatSpinner) getView().findViewById(R.id.spinnerFileType)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    docType = fileTypesList.get(position).getDocumentTypeCode();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void opneFilePicker() {

        Intent i = new Intent(getContext(), FilePickerActivity.class);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);
        i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

        startActivityForResult(i, PICK_FILE_REQUEST_CODE);

    }

    private void openGallery() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), SELECT_PICTURE);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void openCamera() {


        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = timeStamp + ".jpg";
            File storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            pictureImagePath = storageDir.getAbsolutePath() + "/" + imageFileName;
            File file = new File(pictureImagePath);
            Uri outputFileUri = Uri.fromFile(file);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

            getActivity().startActivityForResult(
                    cameraIntent,
                    TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void togglePlaneVisibility() {

        try {
            fileOptionsContainer = (RelativeLayout) getView().findViewById(R.id.fileOptionsContainer);
            fileDetailsContainer = (LinearLayout) getView().findViewById(R.id.fileDetailsContainer);

            fileDetailsContainer.setVisibility(isFileSelected ? View.VISIBLE : View.INVISIBLE);

            fileOptionsContainer.setVisibility(isFileSelected ? View.INVISIBLE : View.VISIBLE);

            if (isFileSelected) {
                switch (fileType) {
                    case FILE_TYPE_IMAGE:
                        txtFileName.setText(filePath);
                        if (filePath != null) {
                            try {
                                new Thread(new ImageLoaderThread(new Handler(), imgFileTypeIcon, filePath, txtFileName)).start();

                            } catch (Exception e) {
                            }
                        }
                        break;
                    case FILE_TYPE_IMAGE_GIF:
                        try {
                            File file = new File(filePath);

                            txtFileName.setText(FilenameUtils.getName(file.getAbsolutePath()));

                            imgFileTypeIcon.setImageResource(R.drawable.gif);

                            txtFilePath.setText(file.getPath());

                        } catch (Exception e) {

                        }
                        break;
                    case FILE_TYPE_VIDEO:
                        try {

                            File file = new File(filePath);

                            txtFileName.setText(FilenameUtils.getName(file.getAbsolutePath()));

                            imgFileTypeIcon.setImageResource(R.drawable.vide);

                            txtFilePath.setText(file.getPath());

                        } catch (Exception e) {

                        }
                        break;
                    case FILE_TYPE_IMAGE_JPG:
                        try {

                            File file = new File(filePath);

                            txtFileName.setText(FilenameUtils.getName(file.getAbsolutePath()));

                            imgFileTypeIcon.setImageResource(R.drawable.jpeg);

                            txtFilePath.setText(file.getPath());

                        } catch (Exception e) {

                        }
                        break;

                    case FILE_TYPE_WORD:
                        try {

                            File file = new File(filePath);

                            txtFileName.setText(FilenameUtils.getName(file.getAbsolutePath()));

                            imgFileTypeIcon.setImageResource(R.drawable.word);

                            txtFilePath.setText(file.getPath());

                        } catch (Exception e) {

                        }
                        break;

                    case FILE_TYPE_TXT:
                        try {

                            File file = new File(filePath);

                            txtFileName.setText(FilenameUtils.getName(file.getAbsolutePath()));

                            imgFileTypeIcon.setImageResource(R.drawable.txt);

                            txtFilePath.setText(file.getPath());

                        } catch (Exception e) {

                        }
                        break;
                    case FILE_TYPE_UNKNOWN:
                        try {

                            File file = new File(filePath);

                            txtFileName.setText(FilenameUtils.getName(file.getAbsolutePath()));

                            imgFileTypeIcon.setImageResource(R.drawable.file);

                            txtFilePath.setText(file.getPath());

                        } catch (Exception e) {

                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case SELECT_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    isFileSelected = true;
                    fileType = FILE_TYPE_IMAGE;
                    filePath = getPath(data.getData());

                    togglePlaneVisibility();
                }
                break;
            case PICK_FILE_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    isFileSelected = true;

                    try {

                        ContentResolver cR = getContext().getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        type = mime.getExtensionFromMimeType(cR.getType(data.getData()));

                        if (type == null) {

                            type = FilenameUtils.getExtension(new File(data.getData().getPath()).getPath());

                        }

                        //jpg , png , gif , mp4 , pdf , mp4 , docx , xls , txt


                        if (type != null) {
                            filePath = new File(data.getData().getPath()).getPath();
                            if (type.equals("jpg")) {
                                fileType = FILE_TYPE_IMAGE_JPG;
                            } else if (type.equals("png")) {
                                fileType = FILE_TYPE_IMAGE_PNG;
                            } else if (type.equals("gif")) {
                                fileType = FILE_TYPE_IMAGE_GIF;
                            } else if (type.equals("mp4")) {
                                fileType = FILE_TYPE_VIDEO;
                            } else if (type.equals("docx")) {
                                fileType = FILE_TYPE_WORD;
                            } else if (type.equals("txt")) {
                                fileType = FILE_TYPE_TXT;
                            } else if (type.equals("pdf")) {
                                fileType = FILE_TYPE_PDF;
                            } else {
                                fileType = FILE_TYPE_UNKNOWN;
                            }

                            togglePlaneVisibility();
                        }

                    } catch (Exception e) {

                    }

                }
        }
    }

    /**
     * helper to retrieve the path of an image URI
     */
    public String getPath(Uri uri) {
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isFileSelected", isFileSelected);
        outState.putString("description", ((TextInputEditText) getView().findViewById(R.id.edtTxtDescription)).getText().toString());
        outState.putInt("fileType", this.fileType);
        outState.putString("filePath", this.filePath);
        outState.putString("docType", this.docType);
        outState.putInt("position", ((AppCompatSpinner) getView().findViewById(R.id.spinnerFileType)).getSelectedItemPosition());

        if (this.idString == null) {
            this.idString = getTimeStamp();
        }
        outState.putString("idString", this.idString);
        if (this.fragmentIneractor != null)
            this.fragmentIneractor.onSavedInstanceState(position, this.getIdString());
        this.selectedItemposition = ((AppCompatSpinner) getView().findViewById(R.id.spinnerFileType)).getSelectedItemPosition();
        this.description = ((TextInputEditText) getView().findViewById(R.id.edtTxtDescription)).getText().toString();
        this.fileName = txtFileName.getText().toString();
        if(fileTypesList.size() >0)
            this.docType = fileTypesList.get(((AppCompatSpinner) getView().findViewById(R.id.spinnerFileType)).getSelectedItemPosition()).getDocumentTypeCode();

        Log.e("onSaveInstanceState " + position, this.idString);

    }

    public SelectedFileModel getSelectedFileModel() {
        SelectedFileModel selectedFileModel = null;
        try {
            selectedFileModel = new SelectedFileModel();
            selectedFileModel.setFileSelected(this.isFileSelected);
            selectedFileModel.setFileDescription(description);
            selectedFileModel.setFileMimeType(this.type);
            selectedFileModel.setFileName(this.fileName);
            selectedFileModel.setFilePath(this.filePath);
            selectedFileModel.setFileTypeCode(this.docType);
            selectedFileModel.setFile_counter("1");

            try {
                Uri uri = Uri.fromFile(new File(this.filePath));
                String mime = getMimeType(uri);
                selectedFileModel.setFileMimeType(mime == null ? "" : mime);
            } catch (Exception e) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return selectedFileModel;
    }

    public String getMimeType(Uri uri) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = getContext().getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    public String getIdString() {
        return idString;
    }


    private String getTimeStamp() {
        Long tsLong = System.currentTimeMillis() / 1000;
        return tsLong.toString();
    }


    public interface FragmentIneractor {
        boolean isIdRecycled(String id);

        void onSavedInstanceState(int position, String frgmentId);
    }

}
