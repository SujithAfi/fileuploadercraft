package com.afiinfotech.solverfileuploader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afiinfotech.solverfileuploader.Model.PatientData;
import com.afiinfotech.solverfileuploader.Model.SelectedFileModel;
import com.afiinfotech.solverfileuploader.Utilis.Utilities;
import com.afiinfotech.solverfileuploader.dialogs.UploadDialog;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Response;


/**
 * Created by afi-mac-001 on 27/08/16.
 */
public class UploadUserPhotoActivity extends AppCompatActivity implements UploadDialog.DialogActionListner {

    private String patientID;
    private String userID;
    private String patientName;
    private String patientGender;
    private TextInputEditText etPatientId;
    private TextView tvUserId;
    private TextView tvPatientName;
    private TextView tvPatientGender;
    private PatientData patientData;

    private static final int SELECT_PICTURE = 22;
    public static final int TAKE_PICTURE = 1;
    private static final int FILE_TYPE_IMAGE = 1;
    private int fileType = -1;
    private String pictureImagePath;
    boolean isFileSelected = false;
    private String filePath = null;
    private Handler uiHandler;
    private FrameLayout flImageContainer;
    private ImageView ivUserImage;
    private ImageView ivClose;
    private RelativeLayout rlOptionsContainer;
    private FloatingActionButton fabUpload;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private boolean showcompleteFlag = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_photo_activity);

        etPatientId = (TextInputEditText) findViewById(R.id.edtTxtPatientId);
        tvUserId = (TextView) findViewById(R.id.txtUserId);
        tvPatientName = (TextView) findViewById(R.id.txtUsername);
        tvPatientGender = (TextView) findViewById(R.id.txtGender);
        flImageContainer = (FrameLayout) findViewById(R.id.fl_image);
        ivUserImage = (ImageView) findViewById(R.id.iv_pimage);
        ivClose = (ImageView) findViewById(R.id.iv_close);
        rlOptionsContainer = (RelativeLayout) findViewById(R.id.fileOptionsContainer);
        fabUpload = (FloatingActionButton) findViewById(R.id.btnUpload);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle b = getIntent().getBundleExtra("bundle");

        if(b != null){

            patientID = b.getString("pID");
            userID = b.getString("uID");
            patientName = b.getString("pName");
            patientGender = b.getString("pGender");

            if(patientID != null)
                etPatientId.setText(patientID);
            if(userID != null)
                tvUserId.setText(userID);
            if(patientName != null)
                tvPatientName.setText(patientName);
            if(patientGender != null)
                tvPatientGender.setText(patientGender);

            if(patientID != null && patientName != null && userID != null) {
                PatientData pdata = new PatientData();
                pdata.setPatientId(patientID);
                pdata.setPatientName(patientName);
                pdata.setUserid(userID);
                pdata.setPatientSex(patientGender);

                this.patientData = pdata;
            }

        }

        findViewById(R.id.btnLoad).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utilities.hideKeyboard(UploadUserPhotoActivity.this);
                if (etPatientId.getText().toString().trim().length() > 0)
                    if(Utilities.isNetworkConnected(UploadUserPhotoActivity.this))
                        loadData(etPatientId.getText().toString());
                    else
                        Utilities.showSnackBar(getResources().getString(R.string.no_connectivity), UploadUserPhotoActivity.this);

                else {
                    etPatientId.setError("Enter Patient file id");
                    etPatientId.requestFocus();
                    Utilities.showSnackBar("Enter Patient file id", UploadUserPhotoActivity.this);
                }

            }
        });

        findViewById(R.id.btnOpenCamera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });

        findViewById(R.id.btnOpenGallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openGallery();

            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                flImageContainer.setVisibility(View.GONE);
                rlOptionsContainer.setVisibility(View.VISIBLE);
                fabUpload.setVisibility(View.GONE);
            }
        });

        fabUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utilities.hideKeyboard(UploadUserPhotoActivity.this);
                showcompleteFlag = true;
                if (patientData == null) {
                    new AlertDialog.Builder(UploadUserPhotoActivity.this)
                            .setTitle("Validation error")
                            .setMessage("Patient not selected,please load patient data by providing patient id")
                            .setPositiveButton("ok", null)
                            .setCancelable(false)
                            .create()
                            .show();
                } else {
                    if(Utilities.isNetworkConnected(UploadUserPhotoActivity.this)){

                    if (filePath != null) {


                        SelectedFileModel selectedFileModel = null;
                        try {
                            selectedFileModel = new SelectedFileModel();
                            selectedFileModel.setFileSelected(isFileSelected);
                            selectedFileModel.setFileDescription("");
                            selectedFileModel.setFileMimeType("");
                            selectedFileModel.setFileName("");
                            selectedFileModel.setFilePath(filePath);
                            selectedFileModel.setFileTypeCode("RIMG_PHO");
                            selectedFileModel.setFile_counter("1");

                            try {
                                Uri uri = Uri.fromFile(new File(filePath));
                                String mime = getMimeType(uri);
                                selectedFileModel.setFileMimeType(mime == null ? "" : mime);
                            } catch (Exception e) {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        List<SelectedFileModel> selectedFileModels = new ArrayList<SelectedFileModel>();
                        if (selectedFileModel != null)
                            selectedFileModels.add(selectedFileModel);
                        if (selectedFileModels.isEmpty()) {
                            new AlertDialog.Builder(UploadUserPhotoActivity.this)
                                    .setTitle("User Image Empty")
                                    .setMessage("Please select an image")
                                    .setPositiveButton("OK", null)
                                    .create()
                                    .show();
                        } else {

                            uploadFiles(selectedFileModels);
                        }
                    }
                }
                    else
                        Utilities.showSnackBar(getResources().getString(R.string.no_connectivity), UploadUserPhotoActivity.this);


                }


            }
        });

    }

    public String getMimeType(Uri uri) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = this.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }


    private void uploadFiles(List<SelectedFileModel> selectedFileModels) {

        UploadDialog.create(executorService,
                selectedFileModels,
                ((EditText) findViewById(R.id.edtTxtPatientId)).getText().toString(),
                Utilities.getSharedPreferences(this).getString("user_id", ""), this)
                .show(getSupportFragmentManager(), "Upload Dialog");
    }


    private void loadData(final String patiendId) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Fetching user data");
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    final Response<PatientData> response = Utilities.getWebApi(UploadUserPhotoActivity.this).getPatientData(patiendId).execute();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();

                            if (response == null) {
                                Toast.makeText(UploadUserPhotoActivity.this, "Network error", Toast.LENGTH_SHORT).show();

                            } else if (response.body() == null) {
                                Toast.makeText(UploadUserPhotoActivity.this, "Network error", Toast.LENGTH_SHORT).show();

                            } else if (response.body().getPatientId() == null) {
                                Toast.makeText(UploadUserPhotoActivity.this, "Invalid patient id", Toast.LENGTH_SHORT).show();
                            } else {
                                initUiWithData(response.body());
                            }

                        }
                    });

                } catch (IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                }
            }
        }).start();

    }

    private void initUiWithData(PatientData body) {

        this.patientData = body;

        ((TextView) findViewById(R.id.txtUserId)).setText(body.getUserid());
        ((TextView) findViewById(R.id.txtUsername)).setText(body.getPatientName());
        ((TextView) findViewById(R.id.txtGender)).setText(body.getPatientSex().contains("F") ? "Female" : "Male");
        findViewById(R.id.txtUsername).setSelected(true);

    }


    private void openGallery() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), SELECT_PICTURE);

    }

    private void openCamera() {


        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = timeStamp + ".jpg";
            File storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            pictureImagePath = storageDir.getAbsolutePath() + "/" + imageFileName;
            File file = new File(pictureImagePath);
            Uri outputFileUri = Uri.fromFile(file);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

            startActivityForResult(
                    cameraIntent,
                    TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case SELECT_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    isFileSelected = true;
                    fileType = FILE_TYPE_IMAGE;
                    filePath = getPath(data.getData());
                    togglePlaneVisibility(filePath);

                }
                break;
            case TAKE_PICTURE :
                if(resultCode == Activity.RESULT_OK) {
                    try {
                        if (pictureImagePath != null) {
                            isFileSelected = true;
                            fileType = FILE_TYPE_IMAGE;
                            filePath = pictureImagePath;
                            pictureImagePath = null;
                            togglePlaneVisibility(filePath);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

        }
    }

    private void togglePlaneVisibility(String filePath) {

        flImageContainer.setVisibility(View.VISIBLE);
        rlOptionsContainer.setVisibility(View.GONE);
        fabUpload.setVisibility(View.VISIBLE);


        try {
            File imgFile = new  File(filePath);

            if(imgFile.exists()){

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                ImageView myImage = (ImageView) findViewById(R.id.iv_pimage);

                myImage.setImageBitmap(myBitmap);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * helper to retrieve the path of an image URI
     */
    public String getPath(Uri uri) {
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = this.managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCompletedAllUploads() {
        if (showcompleteFlag)
            Utilities.showSnackBar("User Photo upload Completed.", UploadUserPhotoActivity.this);

        flImageContainer.setVisibility(View.GONE);
        rlOptionsContainer.setVisibility(View.VISIBLE);
        fabUpload.setVisibility(View.GONE);
        filePath = null;

    }

    @Override
    public void onFailed() {

    }
}
