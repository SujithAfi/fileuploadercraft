package com.afiinfotech.solverfileuploader;

import android.app.Application;

import com.afiinfotech.solverfileuploader.Utilis.PostCaptureListener;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by afi-mac-001 on 13/07/16.
 */
@ReportsCrashes(formKey = "", // will not be used
        mailTo = "sujith@afiinfotech.com", mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast)
public class FileUploadApplication extends Application {


    private PostCaptureListener onCaptureComplete;

    public void onCreate() {
        super.onCreate();

        ACRA.init(this);
    }

    public PostCaptureListener getOnCapturCompleteListener() {
        return this.onCaptureComplete;
    }

    public void setOnCaptureCompleteListener(PostCaptureListener onCaptureComplete) {
        this.onCaptureComplete = onCaptureComplete;
    }
}
