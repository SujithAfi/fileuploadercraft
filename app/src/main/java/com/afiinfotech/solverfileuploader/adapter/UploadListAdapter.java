package com.afiinfotech.solverfileuploader.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afiinfotech.solverfileuploader.Model.UploadProgressData;
import com.afiinfotech.solverfileuploader.R;
import com.afiinfotech.solverfileuploader.Utilis.UploadProgressListner;

import java.util.List;

/**
 * Created by afi-mac-001 on 07/06/16.
 */
public class UploadListAdapter extends RecyclerView.Adapter<UploadListAdapter.UploadListAdapterVh> implements UploadProgressListner {


    private List<UploadProgressData> uploadProgressDatas;

    public UploadListAdapter(List<UploadProgressData> uploadProgressDatas) {
        this.uploadProgressDatas = uploadProgressDatas;
    }

    @Override
    public UploadListAdapterVh onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UploadListAdapterVh(LayoutInflater.from(parent.getContext()).inflate(R.layout.child_upload_list, parent, false));
    }

    @Override
    public void onBindViewHolder(UploadListAdapterVh holder, int position) {

        holder.progressUploadSize.setProgress(uploadProgressDatas.get(position).getProgress());
        holder.txtFileName.setText(uploadProgressDatas.get(position).getFileName());
        holder.txtProgress.setText(String.valueOf(uploadProgressDatas.get(position).getProgress()) + "%");
        holder.imgStatus.setVisibility(uploadProgressDatas.get(position).isFailed() || uploadProgressDatas.get(position).isCompleted() ? View.VISIBLE : View.INVISIBLE);
        holder.imgStatus.setImageResource(uploadProgressDatas.get(position).isFailed() ? R.drawable.cancel : uploadProgressDatas.get(position).isCompleted() ? R.drawable.ok : R.drawable.ok
        );
    }

    @Override
    public int getItemCount() {
        return uploadProgressDatas.size();
    }


    @Override
    public void onItemCompleted(int id) {
        for (UploadProgressData uploadProgressData : this.uploadProgressDatas) {
            if (uploadProgressData.getItemId() == id) {
                // uploadProgressData.setProgress(100);
                uploadProgressData.setCompleted(true);
            }
        }
        this.notifyDataSetChanged();
    }

    @Override
    public void onItemProgressChanged(int id, int progress) {
        for (UploadProgressData uploadProgressData : this.uploadProgressDatas) {
            if (uploadProgressData.getItemId() == id) {
                uploadProgressData.setProgress(progress);
                break;
            }
        }
        this.notifyDataSetChanged();
    }

    @Override
    public void onItemUploadError(int id) {
        for (UploadProgressData uploadProgressData : this.uploadProgressDatas) {
            if (uploadProgressData.getItemId() == id) {
                uploadProgressData.setFailed(true);
                break;
            }
        }
        this.notifyDataSetChanged();
    }


    public static class UploadListAdapterVh extends RecyclerView.ViewHolder {

        private ProgressBar progressUploadSize;
        private TextView txtFileName;
        private TextView txtProgress;

        private ImageView imgStatus;

        public UploadListAdapterVh(View itemView) {
            super(itemView);

            progressUploadSize = (ProgressBar) itemView.findViewById(R.id.progressUploadSize);
            txtFileName = (TextView) itemView.findViewById(R.id.txtFileName);
            txtProgress = (TextView) itemView.findViewById(R.id.txtProgress);
            imgStatus = (ImageView) itemView.findViewById(R.id.imgStatus);
        }
    }

}
