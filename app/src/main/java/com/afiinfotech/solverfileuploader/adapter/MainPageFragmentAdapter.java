package com.afiinfotech.solverfileuploader.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.afiinfotech.solverfileuploader.Model.SelectedFileModel;
import com.afiinfotech.solverfileuploader.fragments.FileFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by afi-mac-001 on 06/06/16.
 */
public class MainPageFragmentAdapter extends FragmentStatePagerAdapter implements FileFragment.FragmentIneractor {

    int position;
    private int tabSize = 1;
    private long baseId = 23234;
//    private int tabMaximumSize = 6;

    private FragmentManager fragmentManager;

    private List<String> removedFragmentIds = new ArrayList<>();

    private Map<Integer, FileFragment> fragmentMap = new HashMap<>();

    private Map<Integer, String> positionIdMap = new HashMap<>();

    private DialogActionListner dialogActionListner;

    public MainPageFragmentAdapter(FragmentManager fm, DialogActionListner dialogActionListner) {
        super(fm);
        this.fragmentManager = fm;
        this.dialogActionListner = dialogActionListner;
    }

    public void addTab() {
//        if(tabSize < tabMaximumSize) {
            tabSize++;
            this.notifyDataSetChanged();
//        }
    }

    public void Update() {
        this.notifyDataSetChanged();
    }

    public int getTabSize() {
        return tabSize;
    }

    public void setTabSize(int tabSize) {
        this.tabSize = tabSize;
    }

    public List<SelectedFileModel> getFileModelList() {
        List<SelectedFileModel> selectedFileModels = new ArrayList<>();

        for (Map.Entry<Integer, FileFragment> entry : fragmentMap.entrySet()) {
            if (entry != null) {
                SelectedFileModel selectedFileModel = entry.getValue().getSelectedFileModel();
                if (selectedFileModel.isFileSelected()) {
                    selectedFileModels.add(selectedFileModel);
                }
            }

        }
        return selectedFileModels;
    }

    public void clearFragments() {

        for (Map.Entry<Integer, FileFragment> entry : fragmentMap.entrySet()) {
            this.fragmentManager.beginTransaction().remove(entry.getValue()).commit();
        }
        fragmentMap.clear();
    }

    @Override
    public Fragment getItem(int position) {
        FileFragment fileFragment = new FileFragment();
        fileFragment.setFragmentIneractor(this);
        fileFragment.position = position;
        fragmentMap.put(position, fileFragment);
        return fileFragment;
    }

    @Override
    public int getCount() {
        return this.tabSize;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Attachment " + (position + 1);
    }


    public void removeFragmentAtIndex(int index) {
        if (tabSize > 1) {
            FileFragment removedFragment = this.fragmentMap.remove(index);
            if (removedFragment != null) {
                String indetifier = this.positionIdMap.get(index);
                if (indetifier != null)
                    removedFragmentIds.add(indetifier);
                //fragmentManager.beginTransaction().remove(removedFragment).commit();
                tabSize--;
                notifyDataSetChanged();
            }
        } else {

            dialogActionListner.onCompletedAllUploads();

        }
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public boolean isIdRecycled(String id) {

        if (id == null) {
            return false;
        }
        for (String removedId : this.removedFragmentIds) {
            if (id.equals(removedId))
                return true;
        }
        return false;
    }

    @Override
    public void onSavedInstanceState(int position, String frgmentId) {
        this.positionIdMap.put(position, frgmentId);
    }

    private String getTimeStamp() {
        Long tsLong = System.currentTimeMillis() / 1000;
        return tsLong.toString();
    }

    public interface DialogActionListner {
        void onCompletedAllUploads();

        void onFailed();
    }

}
