package com.afiinfotech.solverfileuploader;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afiinfotech.solverfileuploader.Utilis.Utilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import retrofit2.Response;

public class ConfigurationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Configuration");

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                final String url = ((TextInputEditText) findViewById(R.id.edtTxtApiUrl)).getText().toString().endsWith("/") ? ((TextInputEditText) findViewById(R.id.edtTxtApiUrl)).getText().toString() : ((TextInputEditText) findViewById(R.id.edtTxtApiUrl)).getText().toString() + "/";

                if (url.isEmpty()) {
                    Snackbar.make(view, "Empty URL", Snackbar.LENGTH_LONG).show();
                } else {

                    try {
                        URL urlSt = new URL(url);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        Snackbar.make(view, "Invalid URL", Snackbar.LENGTH_LONG).show();
                        return;
                    }

                    final ProgressDialog progressDialog = new ProgressDialog(ConfigurationActivity.this);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Checking web api");
                    progressDialog.show();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            double first = new Random().nextDouble();
                            double second = new Random().nextDouble();

                            try {
                                Response<Double> result = Utilities.getWebApiForTesting(url).checkWebApi(first, second).execute();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();
                                    }
                                });
                                boolean error = true;
                                if (result.body() != null) {
                                    error = Math.pow(first + second, 2) != result.body().doubleValue();
                                }
                                if (error) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ConfigurationActivity.this, "Invalid API", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            Utilities.getSharedPreferences(ConfigurationActivity.this).edit().putString("api_url", url).commit();

                                            Snackbar.make(view, "Updated Url", Snackbar.LENGTH_LONG)
                                                    .setAction("OK", new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            finish();
                                                        }
                                                    }).show();
                                        }
                                    });
                                }
                            } catch (IOException e) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();
                                        Toast.makeText(ConfigurationActivity.this, "Network error", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    }).start();


                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ((TextInputEditText) findViewById(R.id.edtTxtApiUrl)).setText(Utilities.getWebApiBaseUrl(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
