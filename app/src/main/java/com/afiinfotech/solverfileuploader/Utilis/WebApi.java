package com.afiinfotech.solverfileuploader.Utilis;

import com.afiinfotech.solverfileuploader.Model.AuthenticationRespose;
import com.afiinfotech.solverfileuploader.Model.PatientData;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by afi-mac-001 on 06/06/16.
 */
public interface WebApi {

    @POST("user")
    Call<AuthenticationRespose> login(@Body Map<String, String> variables);

    @GET("patient/{id}")
    Call<PatientData> getPatientData(@Path("id") String id);

    @GET("checkApi")
    Call<Double> checkWebApi(@Query("first") double first, @Query("second") double second);

}
