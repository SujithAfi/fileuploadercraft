package com.afiinfotech.solverfileuploader.Utilis;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.afiinfotech.solverfileuploader.Model.FileTypes;
import com.afiinfotech.solverfileuploader.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by afi-mac-001 on 06/06/16.
 */
public class Utilities {


    public static final String WEB_API_BASE_URL =  "http://192.168.2.161:85/api/";
//    "http://192.168.1.2:84/api/"
//            "http://afisg001.cloudapp.net:82/api/"; cloud
//            "http://192.168.16.63:86/apiFileUploader/api/"; arun
//            "http://85.154.38.78:84/apiFileUploader/api/";   Client
//            "http://192.168.2.3:86/apiFileUploader/api/"; Local
    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";

    public static WebApi getWebApi(Context context) {
        return new Retrofit.Builder()
                .baseUrl(getWebApiBaseUrl(context))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WebApi.class);
    }

    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences("AppData", Context.MODE_PRIVATE);
    }

    public static void saveFileTypes(SharedPreferences.Editor editor, List<FileTypes> fileTypes) {
        editor.putString("fileTypes", new Gson().toJson(fileTypes));
    }

    public static List<FileTypes> getFileTypes(Context context) {
        List<FileTypes> fileTypes = new ArrayList<>();

        try {
            fileTypes = new Gson().fromJson(getSharedPreferences(context).getString("fileTypes", "{}"), new TypeToken<List<FileTypes>>() {
            }.getType());

        } catch (Exception e) {
        }

        return fileTypes;
    }

    public static String getWebApiBaseUrl(Context context) {
        return getSharedPreferences(context).getString("api_url", WEB_API_BASE_URL);
    }

    public static WebApi getWebApiForTesting(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WebApi.class);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showSnackBar(String messsage, Activity activity) {
        Snackbar snack = Snackbar.make(activity.findViewById(android.R.id.content), messsage, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        view.setBackgroundColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.colorPrimary));
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        if (tv != null) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
            tv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        snack.show();
    }

    public static boolean isNetworkConnected(Context context) {
        return ((ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

}
