package com.afiinfotech.solverfileuploader.Utilis;

/**
 * Created by afi-mac-001 on 07/06/16.
 */
public interface UploadProgressListner {

    void onItemCompleted(int id);

    void onItemProgressChanged(int id, int progress);

    void onItemUploadError(int id);


}