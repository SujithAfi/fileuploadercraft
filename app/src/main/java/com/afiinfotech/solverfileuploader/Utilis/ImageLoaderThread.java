package com.afiinfotech.solverfileuploader.Utilis;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

/**
 * Created by afi-mac-001 on 09/06/16.
 */
public class ImageLoaderThread implements Runnable {

    private Handler uiHandler;
    private ImageView imageView;
    private String filepth;
    private TextView textView;

    public ImageLoaderThread(Handler uiHandler, ImageView imageView, String filepth, TextView textView) {
        this.uiHandler = uiHandler;
        this.imageView = imageView;
        this.filepth = filepth;
        this.textView = textView;
    }

    @Override
    public void run() {

        try {
            final File file = new File(this.filepth);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            final Bitmap bitmap = BitmapFactory.decodeFile(this.filepth, options);
            final Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 120, 120, false);

            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    imageView.setImageBitmap(scaled);
                    textView.setText(file.getName());
                }
            });
        } catch (Exception e) {
        }

    }
}
