package com.afiinfotech.solverfileuploader.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by afi-mac-001 on 06/06/16.
 */
public class PatientData {

    @SerializedName("Usr_Id")
    private String userid;

    @SerializedName("PAT_ID")
    private String patientId;

    @SerializedName("PAT_NAME")
    private String patientName;

    @SerializedName("PAT_SNAME")
    private String patientSurName;

    @SerializedName("PAT_SEX")
    private String patientSex;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientSurName() {
        return patientSurName;
    }

    public void setPatientSurName(String patientSurName) {
        this.patientSurName = patientSurName;
    }

    public String getPatientSex() {
        return patientSex;
    }

    public void setPatientSex(String patientSex) {
        this.patientSex = patientSex;
    }
}
