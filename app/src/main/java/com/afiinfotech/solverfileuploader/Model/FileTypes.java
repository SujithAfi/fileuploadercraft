package com.afiinfotech.solverfileuploader.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by afi-mac-001 on 06/06/16.
 */
public class FileTypes {

    @SerializedName("DocType_Cd")
    private String documentTypeCode;

    @SerializedName("DocType_Name")
    private String documentTypeName;

    @SerializedName("IsActive")
    private String isActive;

    public String getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(String documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getDocumentTypeName() {
        return documentTypeName;
    }

    public void setDocumentTypeName(String documentTypeName) {
        this.documentTypeName = documentTypeName;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}
