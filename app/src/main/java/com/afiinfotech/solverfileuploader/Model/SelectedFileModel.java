package com.afiinfotech.solverfileuploader.Model;

/**
 * Created by afi-mac-001 on 07/06/16.
 */
public class SelectedFileModel {

    private boolean isFileSelected;
    private String fileName;
    private String filePath;
    private String fileDescription;
    private String fileMimeType;
    private String fileTypeCode;
    private String file_counter;
    private int id;

    public String getFile_counter() {
        return file_counter;
    }

    public void setFile_counter(String file_counter) {
        this.file_counter = file_counter;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFileSelected() {
        return isFileSelected;
    }

    public void setFileSelected(boolean fileSelected) {
        isFileSelected = fileSelected;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileDescription() {
        return fileDescription;
    }

    public void setFileDescription(String fileDescription) {
        this.fileDescription = fileDescription;
    }

    public String getFileMimeType() {
        return fileMimeType;
    }

    public void setFileMimeType(String fileMimeType) {
        this.fileMimeType = fileMimeType;
    }

    public String getFileTypeCode() {
        return fileTypeCode;
    }

    public void setFileTypeCode(String fileTypeCode) {
        this.fileTypeCode = fileTypeCode;
    }
}
