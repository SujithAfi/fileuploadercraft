package com.afiinfotech.solverfileuploader.Model;

/**
 * Created by afi-mac-001 on 08/06/16.
 */
public class UploadThreadData extends SelectedFileModel {

    private String patientId;
    private String userId;

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
