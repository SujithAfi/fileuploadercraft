package com.afiinfotech.solverfileuploader.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by afi-mac-001 on 06/06/16.
 */
public class AuthenticationRespose {

    @SerializedName("fileTypes")
    List<FileTypes> fileTypesList;
    private boolean authenticated;
    @SerializedName("Usr_Id")
    private String userId;
    @SerializedName("Usr_Name")
    private String userName;

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<FileTypes> getFileTypesList() {
        return fileTypesList;
    }

    public void setFileTypesList(List<FileTypes> fileTypesList) {
        this.fileTypesList = fileTypesList;
    }
}
